#!/bin/bash

# 整体流程 zilongDir => targetDir => submoduleSpacemacsPrivate

# 复制子龙配置到目标文件夹
targetDir="../../../target/latest/spacemacs-private-ubuntu"
zilongDir="../../../submodule/spacemacs-private"
if [ -d "${targetDir}" ]; then
  rm -rf ${targetDir}; 
fi
cp -a ${zilongDir}/ ${targetDir}/

# 修改 zilongshanren 配置
if [ ! -d "${targetDir}/layers/zilongshanren/" ]; then
  mkdir -p ${targetDir}/layers/zilongshanren/
fi
if [ ! -d "${targetDir}/layers/zilongshanren-misc/" ]; then
  mkdir -p ${targetDir}/layers/zilongshanren-misc/
fi
if [ ! -d "${targetDir}/layers/zilongshanren-programming/" ]; then
  mkdir -p ${targetDir}/layers/zilongshanren-programming/
fi
cp ../common/zilongshanren/layers.el ${targetDir}/layers/zilongshanren/layers.el
cp ../common/zilongshanren/config.el ${targetDir}/layers/zilongshanren/config.el
cp ../common/zilongshanren-misc/funcs.el ${targetDir}/layers/zilongshanren-misc/funcs.el
cp ../common/zilongshanren-misc/packages.el ${targetDir}/layers/zilongshanren-misc/packages.el
cp ../common/zilongshanren-programming/funcs.el ${targetDir}/layers/zilongshanren-programming/funcs.el
cp ../common/zilongshanren-programming/packages.el ${targetDir}/layers/zilongshanren-programming/packages.el

# 添加自己的配置
if [ ! -d "${targetDir}/layers/zilongshanren-tomtsang/" ]; then
  mkdir -p ${targetDir}/layers/zilongshanren-tomtsang/
fi
cp ../common/zilongshanren-tomtsang/layers.el ../common/zilongshanren-tomtsang/packages.el ${targetDir}/layers/zilongshanren-tomtsang/
cp ./spacemacs.d/init.el ${targetDir}/init.el
cp ./zilongshanren-ui/config.el ${targetDir}/layers/zilongshanren-ui/config.el 

# 传送到 submodule 
submoduleSpacemacsPrivate="../../../submodule/tom-spacemacs-private"
cd ${targetDir}
cp -rf `ls -a . | grep -E -v "^(.|..|.git|.gitignore)$"` ${submoduleSpacemacsPrivate}
cd - 
