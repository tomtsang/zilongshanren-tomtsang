
## funcs.el

```shell
➜  mac git:(master) ✗ diff ../common/zilongshanren-programming/funcs.el ../../../submodule/spacemacs-private/layers/zilongshanren-programming/funcs.el >> ../common/zilongshanren-programming/README.md
253,254c228
<   (when (my-project-name-contains-substring "tomtsang")
<     ;; tomtsang 为 $HOME的名称
---
>   (when (my-project-name-contains-substring "guanghui")
```

## packages.el 

### 为了react,  tuning the indenting behaviour. 
来自： http://spacemacs.org/layers/+frameworks/react/README.html

修改了 ~/.spacemacs.d/layers/zilongshanren-programming/packages.el

```shell
➜  .spacemacs.d git:(master) ✗ git diff layers/zilongshanren-programming/packages.el
diff --git a/layers/zilongshanren-programming/packages.el b/layers/zilongshanren-programming/packages.el
index b9f73a1..9441ded 100644
--- a/layers/zilongshanren-programming/packages.el
+++ b/layers/zilongshanren-programming/packages.el
@@ -181,7 +181,19 @@
 ;;       (setq inferior-js-program-command "node"))))

 (defun zilongshanren-programming/post-init-web-mode ()
+  (setq-default
+   ;; js2-mode
+   js2-basic-offset 2
+   ;; web-mode
+   css-indent-offset 2
+   web-mode-markup-indent-offset 2
+   web-mode-css-indent-offset 2
+   web-mode-code-indent-offset 2
+   web-mode-attr-indent-offset 2)
   (with-eval-after-load "web-mode"
+    (add-to-list 'web-mode-indentation-params '("lineup-args" . nil))
+    (add-to-list 'web-mode-indentation-params '("lineup-concats" . nil))
+    (add-to-list 'web-mode-indentation-params '("lineup-calls" . nil))
     (web-mode-toggle-current-element-highlight)
     (web-mode-dom-errors-show))
   (setq company-backends-web-mode '((company-dabbrev-code
(END)
➜  .spacemacs.d git:(master) ✗
```

###
