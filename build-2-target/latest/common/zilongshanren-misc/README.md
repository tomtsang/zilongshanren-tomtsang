## funcs.el

### shadowsocks-proxy
```shell
➜  mac git:(master) ✗ diff ../common/zilongshanren-misc ../../../submodule/spacemacs-private-mac/layers/zilongshanren-misc/funcs.el  >> ../common/zilongshanren-misc/README.md
653,665d652
< 
< (define-minor-mode
<   shadowsocks-proxy-mode
<   :global t
<   :init-value nil
<   :lighter " SS"
<   (if shadowsocks-proxy-mode
<       (setq url-gateway-method 'socks)
<     (setq url-gateway-method 'native)))
< 
< (define-global-minor-mode
<   global-shadowsocks-proxy-mode shadowsocks-proxy-mode shadowsocks-proxy-mode
<   :group 'shadowsocks-proxy)
```

### 

## packages.el

### helm-github-stars-username and prodigy-define-service
```shell
➜  mac git:(master) ✗ diff ../common/zilongshanren-misc/packages.el ../../../submodule/spacemacs-private/layers/zilongshanren-misc/packages.el
57,58c93
<       (spacemacs/set-leader-keys "hh" 'highlight-frame-toggle)
<       (spacemacs/set-leader-keys "hc" 'clear-highlight-frame)
---
>
516c585
<       (define-key flyspell-mode-map (kbd "C-;") 'flyspell-correct-previous-word-generic))
---
>       (define-key flyspell-mode-map (kbd "C-;") 'flyspell-correct-previous))
558c629
<     (setq helm-github-stars-username "secretrobots")))
---
>     (setq helm-github-stars-username "zilongshanren")))
871c948
<   (setq persp-kill-foreign-buffer-action 'kill)
---
>   (setq persp-kill-foreign-buffer-behaviour 'kill)
983,1019d1069
<       :name "Hugo Server jane"
<       :command "/usr/local/bin/hugo"
<       :args '("server" "-D" "--navigateToChanged" "-t" "jane")
<       :cwd "~/bitbucket/qqbb/tomtsang-rootsongjc-hugo"
<       :tags '(hugo server)
<       :stop-signal 'sigkill
<       :kill-process-buffer-on-stop t)
<
<     (prodigy-define-service
<       :name "Hugo Server redlounge"
<       :command "/usr/local/bin/hugo"
<       :args '("server" "-D" "--navigateToChanged" "-t" "hugo-redlounge")
<       :cwd "~/bitbucket/qqbb/tomtsang-rootsongjc-hugo"
<       :tags '(hugo server)
<       :stop-signal 'sigkill
<       :kill-process-buffer-on-stop t)
<
<     (prodigy-define-service
<       :name "Hugo Server beautifulhugo"
<       :command "/usr/local/bin/hugo"
<       :args '("server" "-D" "--navigateToChanged" "-t" "beautifulhugo")
<       :cwd "~/bitbucket/qqbb/tomtsang-rootsongjc-hugo"
<       :tags '(hugo server)
<       :stop-signal 'sigkill
<       :init (lambda () (browse-url "http://localhost:1313"))
<       :kill-process-buffer-on-stop t)
<
<     (prodigy-define-service
<       :name "Hugo Server"
<       :command "hugo"
<       :args '("server")
<       :cwd "~/bitbucket/qqbb/tomtsang-rootsongjc-hugo"
<       :tags '(hugo server)
<       :kill-signal 'sigkill
<       :kill-process-buffer-on-stop t)
<
<     (prodigy-define-service
➜  mac git:(master) ✗
```
### 
