
## layers.el

```shell
➜  mac git:(master) ✗ diff ../common/zilongshanren/layers.el ../../../submodule/spacemacs-private/layers/zilongshanren/layers.el
< zilongshanren-tomtsang
```
## config.el  

```shell
➜  mac git:(master) ✗ diff ../common/zilongshanren/config.el ../../../submodule/spacemacs-private/layers/zilongshanren/config.el
12,18c16,18
<  org-agenda-dir "~/org"
<  deft-dir "~/org"
---
>    org-agenda-dir "~/org-notes"
>    deft-dir "~/org-notes"
```

