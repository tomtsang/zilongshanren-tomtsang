#!/bin/bash

cd ~/.spacemacs.d/layers/zilongshanren-tomtsang/
cd build
cp -a ./spacemacs.d-init.el ~/.spacemacs.d/init.el
cp -a ./zilongshanren-layers.el ~/.spacemacs.d/layers/zilongshanren/layers.el
cp -a ./zilongshanren-config.el ~/.spacemacs.d/layers/zilongshanren/config.el
cp -a ./zilongshanren-programming-funcs.el ~/.spacemacs.d/layers/zilongshanren-programming/funcs.el
cp -a ./zilongshanren-misc-packages.el ~/.spacemacs.d/layers/zilongshanren-misc/packages.el
