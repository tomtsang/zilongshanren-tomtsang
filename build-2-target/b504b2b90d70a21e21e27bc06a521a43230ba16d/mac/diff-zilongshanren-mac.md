
## v1

### git status

```shell
On branch tom
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   custom.el
	modified:   init.el
	modified:   layers/zilongshanren-misc/packages.el
	modified:   layers/zilongshanren-programming/funcs.el
	modified:   layers/zilongshanren/config.el
	modified:   layers/zilongshanren/layers.el

Untracked files:
  (use "git add <file>..." to include in what will be committed)

	layers/tom/
	layers/zilongshanren-tomtsang/

no changes added to commit (use "git add" and/or "git commit -a")
```

### git log -2

```shell
commit b504b2b90d70a21e21e27bc06a521a43230ba16d
Author: lionqu <lionqu@tencent.com>
Date:   Fri Nov 2 22:49:02 2018 +0800

    update

commit 01dbf9b6bba87ee17749e787f2320eccc42c7db1
Author: lionqu <lionqu@tencent.com>
Date:   Thu Nov 1 15:44:10 2018 +0800

    add caps lock mode and related keybindings
```

### git diff

```shell
diff --git a/custom.el b/custom.el
index 5cf6095..09ae62f 100644
--- a/custom.el
+++ b/custom.el
@@ -5,7 +5,7 @@
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
- '(ahs-case-fold-search nil)
+ '(ahs-case-fold-search nil t)
  '(command-log-mode-window-size 50)
  '(company-dabbrev-minimum-length 3)
  '(company-dabbrev-other-buffers nil)
@@ -28,6 +28,9 @@
  '(magit-use-overlays nil)
  '(only-global-abbrevs t)
  '(org-agenda-custom-commands nil)
+ '(org-agenda-files
+   (quote
+    ("/Users/tomtsang/org/ubuntu/update-kernel/update-kernel.org" "/Users/tomtsang/org/ubuntu/ubuntu-desktop/index.org" "/Users/tomtsang/org/ubuntu/index.org" "/Users/tomtsang/org/ubuntu/how-to-install-certificates-for-command-line.org" "/Users/tomtsang/org/all-posts.org" "/Users/tomtsang/org/README.org" "/Users/tomtsang/org/golang.org" "/Users/tomtsang/org/journal.org" "/Users/tomtsang/org/gtd.org" "/Users/tomtsang/org/emacs/emacs.org" "/Users/tomtsang/org/emacs/eshell.org" "/Users/tomtsang/org/emacs/yasnippet.org" "/Users/tomtsang/org/inbox.org" "/Users/tomtsang/org/env.org" "/Users/tomtsang/org/k8s/source.org" "/Users/tomtsang/org/k8s/cephfs/index.org" "/Users/tomtsang/org/k8s/storage.org" "/Users/tomtsang/org/k8s/nfs-k8s-yaml.org" "/Users/tomtsang/org/k8s/install/kubeadm-install-faq.org" "/Users/tomtsang/org/k8s/install/index.org" "/Users/tomtsang/org/k8s/upgrade/index.org" "/Users/tomtsang/org/k8s/index.org" "/Users/tomtsang/org/mac.org" "/Users/tomtsang/org/shell/file.org" "/Users/tomtsang/org/notes.org" "/Users/tomtsang/org/sed/sed-string.org" "/Users/tomtsang/org/vim-redo.org" "/Users/tomtsang/org/hugo/themes/jane/index.org" "/Users/tomtsang/org/hugo/hugo-archetypes.org" "/Users/tomtsang/org/hugo/hugo-emacs.org" "/Users/tomtsang/org/hugo/hugo-server-faq.org" "/Users/tomtsang/org/docker/docker-registry/index.org" "/Users/tomtsang/org/docker/docker-install-ubuntu/docker-install-ubuntu.org" "/Users/tomtsang/org/docker/index.org" "/Users/tomtsang/org/git.org")))
  '(org-agenda-ndays 1)
  '(org-agenda-show-all-dates t)
  '(org-agenda-skip-deadline-if-done t)
@@ -36,18 +39,27 @@
  '(org-agenda-start-on-weekday nil)
  '(org-agenda-text-search-extra-files (quote (agenda-archives)))
  '(org-deadline-warning-days 14)
+ '(org-directory "~/bitbucket/qqbb/tomtsang-rootsongjc-hugo/org")
  '(org-fast-tag-selection-single-key (quote expert))
  '(org-log-into-drawer t)
  '(org-pomodoro-play-sounds nil)
  '(org-reverse-note-order t)
  '(package-selected-packages
    (quote
-    (caps-lock doom-modeline eldoc-eval shrink-path ivy-rich prettier-js ivy-yasnippet gitignore-templates evil-goggles sesman dotenv-mode rjsx-mode magit-svn json-navigator hierarchy org-category-capture yasnippet-snippets spaceline-all-the-icons all-the-icons memoize pippel pipenv overseer org-mime nameless ivy-xref ivy-rtags importmagic epc concurrent google-c-style flycheck-rtags evil-cleverparens counsel-gtags counsel-css company-rtags rtags clojure-cheatsheet centered-cursor-mode font-lock+ ghub let-alist seq restclient-helm org-brain sayid evil-lion auctex-latexmk auctex password-generator realgud test-simple loc-changes load-relative company-lua blog-admin string-inflection opencl-mode cuda-mode symon rspec-mode fuzzy browse-at-remote winum helm-swoop unfill highlight-global marshal ht ob-restclient company-restclient know-your-http-well counsel-projectile lispy counsel swiper ivy-purpose hide-comnt helm-purpose window-purpose imenu-list zoutline minitest glsl-mode pug-mode magithub editorconfig dockerfile-mode docker tablist docker-tramp helm-projectile xterm-color shell-pop eshell-z eshell-prompt-extras esh-help graphviz-dot-mode py-isort dumb-jump restclient racket-mode faceup projectile-rails ob-http helm-gtags feature-mode company-auctex rvm ruby-tools ruby-test-mode rubocop robe rbenv rake enh-ruby-mode chruby bundler inf-ruby yapfify sicp helm-mode-manager org origami tiny evil-unimpaired helm-pydoc unicode-whitespace org-projectile github-search flycheck-clojure evil-escape mwim helm-github-stars fcitx solarized-theme tide typescript-mode spaceline powerline org-plus-contrib ivy-hydra helm-gitignore helm-flx helm-descbinds helm-css-scss helm-company helm-ag helm helm-core flyspell-correct-ivy color-identifiers-mode ag bracketed-paste paradox inflections cider names yaml-mode which-key wgrep uuidgen toc-org smex smeargle smartparens reveal-in-osx-finder restart-emacs ranger pytest py-yapf prodigy persp-mode pcre2el osx-trash org-pomodoro mmm-mode markdown-mode lua-mode live-py-mode link-hint launchctl js2-mode jade-mode info+ ibuffer-projectile projectile hy-mode htmlize hl-todo help-fns+ haml-mode gnuplot gitignore-mode github-clone popup git-gutter-fringe+ git-gutter+ flyspell-correct flycheck evil-visual-mark-mode evil-magit magit-popup git-commit with-editor evil-indent-plus iedit evil-ediff evil undo-tree diminish diff-hl ivy tern company column-enforce-mode cmake-mode clojure-snippets eval-sexp-fu pkg-info clojure-mode bind-map bind-key yasnippet auto-compile packed anaconda-mode pythonic ace-window ace-link avy quelpa package-build wrap-region visual-regexp-steroids visual-regexp peep-dired osx-dictionary nodejs-repl litable keyfreq gulpjs find-file-in-project etags-select ctags-update beacon 4clojure moe-theme edn paredit queue peg json-rpc dash-functional web-completion-data makey anzu highlight goto-chg flx gh logito pcache pos-tip guide-key request parent-mode simple-httpd json-snatcher json-reformat multiple-cursors moz ctable orglue epic alert log4e gntp spinner epl hydra async deferred f s chinese-word-at-point dash youdao-dictionary ws-butler web-mode web-beautify volatile-highlights vi-tilde-fringe use-package tagedit smooth-scrolling slim-mode scss-mode sass-mode rfringe reveal-in-finder rainbow-mode rainbow-identifiers rainbow-delimiters pyvenv pyenv-mode popwin pip-requirements persp-projectile pbcopy page-break-lines ox-reveal org-repo-todo org-present org-octopress org-mac-link org-download org-bullets open-junk-file neotree multi-term moz-controller move-text monokai-theme markdown-toc magit macrostep linum-relative leuven-theme less-css-mode json-mode js2-refactor js-doc indent-guide impatient-mode ido-vertical-mode hungry-delete hl-anything highlight-parentheses highlight-numbers highlight-indentation guide-key-tip google-translate golden-ratio github-browse-file gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link gist gh-md ggtags geiser fringe-helper flycheck-ycmd flycheck-pos-tip flx-ido fill-column-indicator fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-tutor evil-terminal-cursor-changer evil-surround evil-search-highlight-persist evil-org evil-numbers evil-nerd-commenter evil-matchit evil-lisp-state evil-jumper evil-indent-textobject evil-iedit-state evil-exchange evil-args evil-anzu engine-mode emmet-mode elisp-slime-nav elfeed discover-my-major deft dash-at-point cython-mode company-ycmd company-web company-tern company-statistics company-quickhelp company-c-headers company-anaconda command-log-mode coffee-mode cmake-font-lock clj-refactor clean-aindent-mode clang-format cider-eval-sexp-fu chinese-fonts-setup buffer-move auto-yasnippet auto-highlight-symbol auto-dictionary align-cljlet aggressive-indent adaptive-wrap ace-jump-mode ac-ispell 2048-game)))
+    (py-autopep8 ein skewer-mode auto-complete websocket elpy godoctor go-tag go-rename go-impl go-guru go-gen-test go-fill-struct go-eldoc flycheck-gometalinter company-go go-mode go chinese-wbim vmd-mode ox-hugo easy-hugo caps-lock doom-modeline eldoc-eval shrink-path ivy-rich prettier-js ivy-yasnippet gitignore-templates evil-goggles sesman dotenv-mode rjsx-mode magit-svn json-navigator hierarchy org-category-capture yasnippet-snippets spaceline-all-the-icons all-the-icons memoize pippel pipenv overseer org-mime nameless ivy-xref ivy-rtags importmagic epc concurrent google-c-style flycheck-rtags evil-cleverparens counsel-gtags counsel-css company-rtags rtags clojure-cheatsheet centered-cursor-mode font-lock+ ghub let-alist seq restclient-helm org-brain sayid evil-lion auctex-latexmk auctex password-generator realgud test-simple loc-changes load-relative company-lua blog-admin string-inflection opencl-mode cuda-mode symon rspec-mode fuzzy browse-at-remote winum helm-swoop unfill highlight-global marshal ht ob-restclient company-restclient know-your-http-well counsel-projectile lispy counsel swiper ivy-purpose hide-comnt helm-purpose window-purpose imenu-list zoutline minitest glsl-mode pug-mode magithub editorconfig dockerfile-mode docker tablist docker-tramp helm-projectile xterm-color shell-pop eshell-z eshell-prompt-extras esh-help graphviz-dot-mode py-isort dumb-jump restclient racket-mode faceup projectile-rails ob-http helm-gtags feature-mode company-auctex rvm ruby-tools ruby-test-mode rubocop robe rbenv rake enh-ruby-mode chruby bundler inf-ruby yapfify sicp helm-mode-manager org origami tiny evil-unimpaired helm-pydoc unicode-whitespace org-projectile github-search flycheck-clojure evil-escape mwim helm-github-stars fcitx solarized-theme tide typescript-mode spaceline powerline org-plus-contrib ivy-hydra helm-gitignore helm-flx helm-descbinds helm-css-scss helm-company helm-ag helm helm-core flyspell-correct-ivy color-identifiers-mode ag bracketed-paste paradox inflections cider names yaml-mode which-key wgrep uuidgen toc-org smex smeargle smartparens reveal-in-osx-finder restart-emacs ranger pytest py-yapf prodigy persp-mode pcre2el osx-trash org-pomodoro mmm-mode markdown-mode lua-mode live-py-mode link-hint launchctl js2-mode jade-mode info+ ibuffer-projectile projectile hy-mode htmlize hl-todo help-fns+ haml-mode gnuplot gitignore-mode github-clone popup git-gutter-fringe+ git-gutter+ flyspell-correct flycheck evil-visual-mark-mode evil-magit magit-popup git-commit with-editor evil-indent-plus iedit evil-ediff evil undo-tree diminish diff-hl ivy tern company column-enforce-mode cmake-mode clojure-snippets eval-sexp-fu pkg-info clojure-mode bind-map bind-key yasnippet auto-compile packed anaconda-mode pythonic ace-window ace-link avy quelpa package-build wrap-region visual-regexp-steroids visual-regexp peep-dired osx-dictionary nodejs-repl litable keyfreq gulpjs find-file-in-project etags-select ctags-update beacon 4clojure moe-theme edn paredit queue peg json-rpc dash-functional web-completion-data makey anzu highlight goto-chg flx gh logito pcache pos-tip guide-key request parent-mode simple-httpd json-snatcher json-reformat multiple-cursors moz ctable orglue epic alert log4e gntp spinner epl hydra async deferred f s chinese-word-at-point dash youdao-dictionary ws-butler web-mode web-beautify volatile-highlights vi-tilde-fringe use-package tagedit smooth-scrolling slim-mode scss-mode sass-mode rfringe reveal-in-finder rainbow-mode rainbow-identifiers rainbow-delimiters pyvenv pyenv-mode popwin pip-requirements persp-projectile pbcopy page-break-lines ox-reveal org-repo-todo org-present org-octopress org-mac-link org-download org-bullets open-junk-file neotree multi-term moz-controller move-text monokai-theme markdown-toc magit macrostep linum-relative leuven-theme less-css-mode json-mode js2-refactor js-doc indent-guide impatient-mode ido-vertical-mode hungry-delete hl-anything highlight-parentheses highlight-numbers highlight-indentation guide-key-tip google-translate golden-ratio github-browse-file gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link gist gh-md ggtags geiser fringe-helper flycheck-ycmd flycheck-pos-tip flx-ido fill-column-indicator fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-tutor evil-terminal-cursor-changer evil-surround evil-search-highlight-persist evil-org evil-numbers evil-nerd-commenter evil-matchit evil-lisp-state evil-jumper evil-indent-textobject evil-iedit-state evil-exchange evil-args evil-anzu engine-mode emmet-mode elisp-slime-nav elfeed discover-my-major deft dash-at-point cython-mode company-ycmd company-web company-tern company-statistics company-quickhelp company-c-headers company-anaconda command-log-mode coffee-mode cmake-font-lock clj-refactor clean-aindent-mode clang-format cider-eval-sexp-fu chinese-fonts-setup buffer-move auto-yasnippet auto-highlight-symbol auto-dictionary align-cljlet aggressive-indent adaptive-wrap ace-jump-mode ac-ispell 2048-game)))
  '(paradox-github-token t)
  '(ring-bell-function (quote ignore))
  '(safe-local-variable-values
    (quote
-    ((eval setenv "PYTHONPATH" "/Users/guanghui/cocos2d-x/tools/cocos2d-console/plugins:/Users/guanghui/cocos2d-x/tools/cocos2d-console/bin"))))
+    ((eval add-hook
+           (quote after-save-hook)
+           (function org-hugo-export-wim-to-md)
+           :append :local)
+     (typescript-backend . tide)
+     (typescript-backend . lsp)
+     (javascript-backend . tern)
+     (javascript-backend . lsp)
+     (eval setenv "PYTHONPATH" "/Users/guanghui/cocos2d-x/tools/cocos2d-console/plugins:/Users/guanghui/cocos2d-x/tools/cocos2d-console/bin"))))
  '(sp-show-pair-from-inside t)
  '(tags-revert-without-query t)
  '(vc-follow-symlinks t)
diff --git a/init.el b/init.el
index 629ac3d..1a66f96 100644
--- a/init.el
+++ b/init.el
@@ -59,12 +59,13 @@ values."
           osx-command-as 'super)
      restclient
      (gtags :disabled-for clojure emacs-lisp javascript latex python shell-scripts)
-     (shell :variables shell-default-shell 'eshell)
+     (shell :variables shell-default-shell 'shell)
      ;; docker
      latex
      deft
      markdown
-     (org :variables org-want-todo-bindings t)
+     (org :variables org-want-todo-bindings t
+          org-enable-hugo-support t)
      gpu
      yaml
      react
@@ -76,8 +77,8 @@ values."
      html
      (javascript :variables javascript-backend 'nil)
      (typescript :variables
-                typescript-fmt-on-save nil
-                typescript-fmt-tool 'typescript-formatter)
+                 typescript-fmt-on-save nil
+                 typescript-fmt-tool 'typescript-formatter)
      emacs-lisp
      (clojure :variables clojure-enable-fancify-symbols t)
      racket
@@ -87,6 +88,10 @@ values."
      (chinese :packages youdao-dictionary fcitx
               :variables chinese-enable-fcitx nil
               chinese-enable-youdao-dict t)
+     (go :variables
+         go-use-gometalinter t
+         gofmt-command "goimports"
+         go-tab-width 4)
      )
    ;; List of additional packages that will be installed without being
    ;; wrapped in a layer. If you need some configuration for these
diff --git a/layers/zilongshanren-misc/packages.el b/layers/zilongshanren-misc/packages.el
index f2db38a..38ce33e 100644
--- a/layers/zilongshanren-misc/packages.el
+++ b/layers/zilongshanren-misc/packages.el
@@ -555,7 +555,7 @@ Search for a search tool in the order provided by `dotspacemacs-search-tools'."
   (use-package helm-github-stars
     :commands (helm-github-stars)
     :init
-    (setq helm-github-stars-username "zilongshanren")))
+    (setq helm-github-stars-username "secretrobots")))
 
 
 
@@ -979,6 +979,43 @@ Search for a search tool in the order provided by `dotspacemacs-search-tools'."
       :kill-signal 'sigkill
       :kill-process-buffer-on-stop t)
 
+    (prodigy-define-service
+      :name "Hugo Server jane"
+      :command "/usr/local/bin/hugo"
+      :args '("server" "-D" "--navigateToChanged" "-t" "jane")
+      :cwd "~/bitbucket/qqbb/tomtsang-rootsongjc-hugo"
+      :tags '(hugo server)
+      :stop-signal 'sigkill
+      :kill-process-buffer-on-stop t)
+
+    (prodigy-define-service
+      :name "Hugo Server redlounge"
+      :command "/usr/local/bin/hugo"
+      :args '("server" "-D" "--navigateToChanged" "-t" "hugo-redlounge")
+      :cwd "~/bitbucket/qqbb/tomtsang-rootsongjc-hugo"
+      :tags '(hugo server)
+      :stop-signal 'sigkill
+      :kill-process-buffer-on-stop t)
+
+    (prodigy-define-service
+      :name "Hugo Server beautifulhugo"
+      :command "/usr/local/bin/hugo"
+      :args '("server" "-D" "--navigateToChanged" "-t" "beautifulhugo")
+      :cwd "~/bitbucket/qqbb/tomtsang-rootsongjc-hugo"
+      :tags '(hugo server)
+      :stop-signal 'sigkill
+      :init (lambda () (browse-url "http://localhost:1313"))
+      :kill-process-buffer-on-stop t)
+
+    (prodigy-define-service
+      :name "Hugo Server"
+      :command "hugo"
+      :args '("server")
+      :cwd "~/bitbucket/qqbb/tomtsang-rootsongjc-hugo"
+      :tags '(hugo server)
+      :kill-signal 'sigkill
+      :kill-process-buffer-on-stop t)
+
     (prodigy-define-service
       :name "Hexo Server"
       :command "hexo"
diff --git a/layers/zilongshanren-programming/funcs.el b/layers/zilongshanren-programming/funcs.el
index 0ff587a..ff97b41 100644
--- a/layers/zilongshanren-programming/funcs.el
+++ b/layers/zilongshanren-programming/funcs.el
@@ -250,7 +250,8 @@ version 2015-08-21"
 
 (defun my-setup-develop-environment ()
   (interactive)
-  (when (my-project-name-contains-substring "guanghui")
+  (when (my-project-name-contains-substring "tomtsang")
+    ;; tomtsang 为 $HOME的名称
     (cond
      ((my-project-name-contains-substring "cocos2d-x")
       ;; C++ project don't need html tags
diff --git a/layers/zilongshanren/config.el b/layers/zilongshanren/config.el
index da57d2f..19de923 100644
--- a/layers/zilongshanren/config.el
+++ b/layers/zilongshanren/config.el
@@ -13,8 +13,8 @@
  deft-dir "f:/org-notes"
  blog-admin-dir "f:/zilongshanren.com")
   (setq
- org-agenda-dir "~/org-notes"
- deft-dir "~/org-notes"
+ org-agenda-dir "~/org"
+ deft-dir "~/org"
  blog-admin-dir "~/zilongshanren.com"))
 
 
diff --git a/layers/zilongshanren/layers.el b/layers/zilongshanren/layers.el
index c2860b3..730b7ff 100644
--- a/layers/zilongshanren/layers.el
+++ b/layers/zilongshanren/layers.el
@@ -13,6 +13,7 @@
                                       zilongshanren-misc
                                       zilongshanren-programming
                                       zilongshanren-ui
+                                      zilongshanren-tomtsang
                                       zilongshanren-org
                                       zilongshanren-better-defaults
                                       ))


```


## v2
