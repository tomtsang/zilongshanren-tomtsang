#!/bin/bash

zilongdir="../../../target/latest/spacemacs-private"
if [ -d "${zilongdir}" ]; then
  rm -rf ${zilongdir}; 
fi
cp -a ../../../submodule/spacemacs-private/ ../../../target/latest/spacemacs-private/

# 修改 zilongshanren 配置
cp ../zilongshanren-layers.el ${zilongdir}/layers/zilongshanren/layers.el
cp ../zilongshanren-config.el ${zilongdir}/layers/zilongshanren/config.el
cp ../zilongshanren-misc-packages.el ${zilongdir}/layers/zilongshanren-misc/packages.el 
cp ../zilongshanren-programming-funcs.el ${zilongdir}/layers/zilongshanren-programming/funcs.el 

# 添加自己的配置
if [ ! -d "${zilongdir}/layers/zilongshanren-tomtsang/" ]; then
  mkdir -p ${zilongdir}/layers/zilongshanren-tomtsang/
fi
cp ../layers.el ../packages.el ${zilongdir}/layers/zilongshanren-tomtsang/
cp ./spacemacs.d-init.el ${zilongdir}/init.el
cp ./zilongshanren-ui-config.el ${zilongdir}/layers/zilongshanren-ui/config.el 
