
这里写一些 与 子龙山人 不同的地方

## v1

### 初次使用子龙山人的配置

```
test2@ubuntu:~/.spacemacs.d$ git remote -v
origin	https://github.com/zilongshanren/spacemacs-private (fetch)
origin	https://github.com/zilongshanren/spacemacs-private (push)
test2@ubuntu:~/.spacemacs.d$ git log -2
commit e47d0a9e18cbf69e3dcb49355a6ec5ab27c2b8fe
Author: zilongshanren <guanghui8827@126.com>
Date:   Sun Jan 6 20:02:41 2019 +0800

    fix python, node, java run-dwim command

commit ec7b70500f0724b9ec576f2d93e7b2aaf8747ccb
Author: zilongshanren <guanghui8827@126.com>
Date:   Fri Jan 4 18:49:29 2019 +0800

    improve compile dwim run command
test2@ubuntu:~/.spacemacs.d$ git diff 
diff --git a/init.el b/init.el
index a7706ae..bcf6e21 100644
--- a/init.el
+++ b/init.el
@@ -118,6 +118,7 @@ This function should only modify configuration layer settings."
                     helm-c-yasnippet ace-jump-helm-line helm-make magithub
                     helm-themes helm-swoop helm-spacemacs-help smeargle
                     ido-vertical-mode flx-ido company-quickhelp ivy-rich helm-purpose
+                   clojure-cheatsheet
                     )
    dotspacemacs-install-packages 'used-only
    dotspacemacs-delete-orphan-packages t))
@@ -159,7 +160,7 @@ It should only modify the values of Spacemacs settings."
 
    ;; Maximum allowed time in seconds to contact an ELPA repository.
    ;; (default 5)
-   dotspacemacs-elpa-timeout 5
+   dotspacemacs-elpa-timeout 300 
 
    ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
    ;; This is an advanced option and should not be changed unless you suspect
diff --git a/layers/zilongshanren-ui/config.el b/layers/zilongshanren-ui/config.el
index 7735481..a0853b4 100644
--- a/layers/zilongshanren-ui/config.el
+++ b/layers/zilongshanren-ui/config.el
@@ -25,23 +25,23 @@
         (:eval (if (buffer-file-name)
                    (abbreviate-file-name (buffer-file-name)) "%b"))))
 
-(define-fringe-bitmap 'right-curly-arrow
-  [#b00000000
-   #b00000000
-   #b00000000
-   #b00000000
-   #b01110000
-   #b00010000
-   #b00010000
-   #b00000000])
+;; (define-fringe-bitmap 'right-curly-arrow
+;;   [#b00000000
+;;    #b00000000
+;;    #b00000000
+;;    #b00000000
+;;    #b01110000
+;;    #b00010000
+;;    #b00010000
+;;    #b00000000])
 
-(define-fringe-bitmap 'left-curly-arrow
-  [#b00000000
-   #b00001000
-   #b00001000
-   #b00001110
-   #b00000000
-   #b00000000
-   #b00000000
-   #b00000000])
+;; (define-fringe-bitmap 'left-curly-arrow
+;;   [#b00000000
+;;    #b00001000
+;;    #b00001000
+;;    #b00001110
+;;    #b00000000
+;;    #b00000000
+;;    #b00000000
+;;    #b00000000])
 
test2@ubuntu:~/.spacemacs.d$ 

```
这样子，可以正常在ubuntu下启动，从而使用 子龙山人 的配置了。

### 使用我的配置

 
