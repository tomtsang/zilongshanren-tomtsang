



target     存放最后生效的文件，依赖于 `build-2-target/*/init.sh` 文件生成 
build-2-target     存放生成工具，主要就是修改的配置文件和init.sh 文件
submodule  存放submodule

如： $/build-2-target/latest/ubuntu/init.sh 会把文件生成到 $/target/latest/spacemacs-private/ 中，那么 $/target/latest/spacemacs-private/ 下的文件，就是最终的 ${HOME}/.spacemacs.d/ 文件夹。

