
基于 [子龙山人的spacemacs配置](https://github.com/zilongshanren/spacemacs-private) ,然后把自己的配置放在这个文件夹中。

----------------------------------------------------

## 安装 

总体安装思路：


1. 拉取本 仓库, 查看README.md，找到对应的应该拉取的  [子龙山人的spacemacs配置](https://github.com/zilongshanren/spacemacs-private) 的版本。 
2. 拉取  [子龙山人的spacemacs配置](https://github.com/zilongshanren/spacemacs-private) ，checkout 到对应版本。
3. 把2个仓库放在正确位置，子龙的是 ~/.spacemacs.d/ , 我的是 ~/.spacemacs.d/layers/zilongshanren-tomtsang/
4. 切换到对应位置，执行./init-m1.sh, 创建硬链接

初次安装思路：

1. 保证 [子龙山人的spacemacs配置](https://github.com/zilongshanren/spacemacs-private) 可以在调整后，安装成功，不报错。
2. 保证 本仓库 配置，成功，不报错。

## 更新

#### 方法1

因为在 build/ 下做的都是硬链接，所以，我们直接可以在build/下对原来的文件做修改。
如果有冲突，则现场修复。

1. 直接拉取 子龙山人的spacemacs配置
2. 拉取本 仓库 
3. 执行./init-m1.sh, 创建硬链接

#### 方法2, 不推荐

先还原成 子龙山人的spacemacs配置
然后拉取 子龙山人的spacemacs配置
到此文件夹，运行 init-m2.sh
重启spacemacs `SPC q R`



## tips

### 避免来回增删

在测试spacemacs配置的过程中，想避免频繁来回增删插件包，可以把某一次成功的效果保存起来。

```shell
cp -a ~/.emacs.d/ ~/.spacemacs.d/ ~/software/spacemacs-d-ok/
```

--------------------------------------------
以下是对不同操作系统的不同操作。每一个操作系统对应的版本可能不同哟！

## 先安装 zilongshanren的 spacemacs-private 成功。 

按照其仓库的安装方法安装。

然后如果说会报错, 则需要像下面这样配置一下。

注意每个系统下载的 zilongshanren 的版本哈！

### ubuntu

如果你的系统是ubuntu，且是初次安装 子龙 的配置，则需要看这一部分。
具体见 ~/.spacemacs.d/layers/zilongshanren-tomtsang/build/ubuntu/README-diff-zilongshanren.md



当然，如果您不想手工输入，可以执行一下下面命令。

### mac os

这里是 mac os 下安装 子龙山人的spacemacs配置。


### tips

如果说您只想使用子龙山人的配置，不想使用我的配置，那操作到这里就可以了。

## 安装 本仓库

### 与子龙的不同 

记录本仓库集成子龙配置时，对子龙配置进行适当的调整




