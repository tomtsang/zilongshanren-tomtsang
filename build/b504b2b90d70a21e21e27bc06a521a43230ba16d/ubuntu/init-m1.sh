#!/bin/bash

cd ../
cp layers.el packages.el ~/.spacemacs.d/layers/zilongshanren-tomtsang/
rm ~/.spacemacs.d/layers/zilongshanren/layers.el && ln ./zilongshanren-layers.el ~/.spacemacs.d/layers/zilongshanren/layers.el
rm ~/.spacemacs.d/layers/zilongshanren/config.el && ln ./zilongshanren-config.el ~/.spacemacs.d/layers/zilongshanren/config.el
rm ~/.spacemacs.d/layers/zilongshanren-misc/packages.el && ln ./zilongshanren-misc-packages.el ~/.spacemacs.d/layers/zilongshanren-misc/packages.el 
rm ~/.spacemacs.d/layers/zilongshanren-programming/funcs.el && ln ./zilongshanren-programming-funcs.el ~/.spacemacs.d/layers/zilongshanren-programming/funcs.el 

cd ./ubuntu/
rm ~/.spacemacs.d/init.el && ln ./spacemacs.d-init.el ~/.spacemacs.d/init.el
rm ~/.spacemacs.d/layers/zilongshanren-ui/config.el && ln ./zilongshanren-ui-config.el ~/.spacemacs.d/layers/zilongshanren-ui/config.el 
